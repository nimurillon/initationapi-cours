from sqlalchemy.orm.session import Session
import Entity.hotel

class HotelRepository():
    def get_hotels(self, db: Session):
        return db.query(Entity.hotel.Hotel).all()