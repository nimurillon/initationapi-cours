from sqlalchemy.sql.schema import Column
from sqlalchemy.sql.sqltypes import Integer, String
from config.bdd import Base

class Hotel(Base):
    __tablename__ = "hotels"
    id = Column(Integer, primary_key=True, index=True)