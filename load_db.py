from Entity.hotel import Hotel
from config.bdd import SessionLocal, engine, Base

db = SessionLocal()

Base.metadata.create_all(bind=engine)

hotels = {
    1: {
        'name': "Hotel Escale Oceania",
        "description": """Si vous recherchez un hôtel familial à Biarritz, alors l'Hotel Escale Oceania Biarritz est le choix idéal.

Grâce à son emplacement tout près des monuments populaires, comme Notre Dame du Rocher (0,1 km) et Eglise Saint-Charles. (0,2 km), les clients de l'Altess Hotel pourront facilement profiter des attractions les plus célèbres de Biarritz.""",
        'ville': 'Biarritz',
        'adresse': '19 ave de la Reine Victoria',
        'codePostal': 64200
    },
    2: {
            'name': "Novotel Clermont-Ferrand",
            "description": """Superbe hôtel avec piscine extérieure, terrasse, jardin et Salle de jeux vidéo.""",
            'ville': 'Clermont-Ferrand',
            'adresse': '34 Rue Georges Besse',
            'codePostal': 63100
    },
    3: {
            'name': "Kyriad Clermont-Ferrand Centre",
            "description": """Si vous recherchez un hôtel familial à Clermont-Ferrand, l'Hôtel Kyriad Clermont-Ferrand Centre est le choix idéal.

Situé à proximité de la place de Jaude, cet hôtel est idéal pour découvrir le centre de Clermont-Ferrand""",
            'ville': 'Clermont-Ferrand',
            'adresse': '51 Rue Bonnabaud',
            'codePostal': 63000
    },
}

for h in hotels:
    db_hotel = Hotel(**hotels[h])
    db.add(db_hotel)
    db.commit()

db.close()