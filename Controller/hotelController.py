from fastapi import APIRouter
from fastapi.params import Depends
from sqlalchemy.orm.session import Session
from config.bdd import get_db
from Repository.hotelRepository import HotelRepository

router = APIRouter(
    prefix="/hotels",
    tags=["hotels"]
)