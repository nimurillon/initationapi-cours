from pydantic import BaseModel

class HotelDTO(BaseModel):
    id: int

    class Config:
        orm_mode = True